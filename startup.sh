#!/bin/sh

RETRY_COUNT=$1

if [ $(($RETRY_COUNT + 0)) -gt $(($RETRY_MAX_COUNT + 0)) ]; then
    exit 0
fi

ckcmd()
{
    type $1 | grep " is " | wc -l
}

# Run VPN if VPN_ENABLE is 1
if [[ $VPN_ENABLE -eq 1 ]];then
  echo "startup/vpn: configuring vpn client."
  # template out all the config files using env vars
  sed -i 's/right=.*/right='$VPN_SERVER'/' /etc/ipsec.conf
  echo ': PSK "'$VPN_PSK'"' > /etc/ipsec.secrets
  sed -i 's/lns = .*/lns = '$VPN_SERVER'/' /etc/xl2tpd/xl2tpd.conf
  sed -i 's/name .*/name '$VPN_USERNAME'/' /etc/ppp/options.l2tpd.client
  sed -i 's/password .*/password '$VPN_PASSWORD'/' /etc/ppp/options.l2tpd.client

  if [ ! $(ckcmd ipsec) -eq 1 ]; then
    sleep infinity
    exit 0
  fi

  rm -f /var/run/xl2tpd.pid

  ipsec restart
  sleep 3
  ipsec up myvpn
  sleep 5

  # startup xl2tpd ppp daemon then send it a connect command
  (sleep 7 \
    && echo "startup/vpn: send connect command to vpn client." \
    && echo "c myvpn" > /var/run/xl2tpd/l2tp-control) &
  exec /reconnector.sh $RETRY_COUNT &
  echo "startup/vpn: start vpn client daemon."
  exec /usr/local/sbin/xl2tpd -p /var/run/xl2tpd.pid -c /etc/xl2tpd/xl2tpd.conf -C /var/run/xl2tpd/l2tp-control -D &
else
  echo "startup/vpn: Ignore vpn client."
fi

# Run socks5 server after 10 Seconds if SOCKS5_ENABLE is 1
sleep 10
if [[ $SOCKS5_ENABLE -eq 1 ]];then
  echo "startup/socks5: waiting for ppp0"
  (while ! route | grep ppp0 > /dev/null; do sleep 1; done \
    && echo "startup/socks5: Socks5 will start in $SOCKS5_START_DELAY seconds" \
    && sleep $SOCKS5_START_DELAY \
    && sockd -N $SOCKS5_FORKS) &
else
  echo "startup/socks5: Ignore socks5 server."
fi

exec tail -f /dev/null  ## %%LAST-CMD_2_REPLACE%
