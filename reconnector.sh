#!/bin/sh

RETRY_COUNT=$1

ckppp() { route | grep  ppp0 | wc -l; }
kill_reconn() { kill $(ps -ef | grep {reconnector.sh} | grep -v grep | awk '{print $1}'); }
restart_script()
{
  exec /startup.sh $(($RETRY_COUNT + 1)) &
  kill_reconn
}

while true; do
  if [ $(ckppp) -eq 0 ]; then
    # wait for 5 sec to make sure ppp0 is avalible again in case of diconnection
    echo "reconnector: wait for 5 secs";sleep 5

    if [ $(ping -q -W 1 -w 1 -c 1 8.8.8.8 | grep "1 packets received" | wc -l) -eq 0 ]; then
      sleep 10
      restart_script
      exit 1
    fi

    # try to connect vpn if it's not connected
    xl2tpd-control -c /var/run/xl2tpd/l2tp-control connect 'myvpn'
    # Wait for vpn to create ppp0 network interface
    count=0
    while [ $count -lt 30 ] && [ $(ckppp) -eq 0 ]; do
      count=$(($count + 1))
      echo "reconnector: waiting for ppp0"; sleep 5;
    done

    if [ $(ckppp) -eq 0 ]; then
      restart_script
      exit 1
    fi

    # Get Default Gateway
    DEFAULT_ROUTE_IP=$(route | grep eth0 | grep default | awk '{print $2}')
    # Get VPN Gateway
    VPN_ROUTE_IP=$(ip a show ppp0  | grep peer | sed -e 's/.*peer\s\+\(\d\+\.\d\+\.\d\+\.\d\+\).*/\1/g')
    # Get IPs of VPN's FQDN if presented
    if echo "${VPN_SERVER}" | grep -E '\d+\.\d+\.\d+\.\d+'; then
      echo "${VPN_SERVER}" > /tmp/all_ips.txt
    else
      dig $VPN_SERVER a | grep "$VPN_SERVER" | grep -E '\d+\.\d+\.\d+\.\d+' | awk '{print $5}' > /tmp/all_ips.txt 
    fi

    echo "reconnector: Default Gateway=$DEFAULT_ROUTE_IP"
    echo "reconnector: VPN Gateway=$VPN_ROUTE_IP"
    echo -e "reconnector: VPN servers: \n$(cat /tmp/all_ips.txt | sed -e 's/^/ - /g')"
    echo "reconnector: wait for 3 secs";sleep 3
    # ip route add $VPN_SERVER via $DEFAULT_ROUTE_IP dev eth0
    while read p; do echo "reconnector: Adding $p to route table...";ip route add $p via $DEFAULT_ROUTE_IP dev eth0; done < /tmp/all_ips.txt

    # Check routes
    route -n
    traceroute 8.8.8.8 -m 1

    # Set default gateway to VPN
    route add -net default gw $VPN_ROUTE_IP dev ppp0

    # Check routes
    route -n
    traceroute 8.8.8.8 -m 1

    # Show Public IP
    # curl icanhazip.com
    echo "reconnector: Your Public IP: $(curl https://api64.ipify.org -s)"

    iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE

    if [ -n "$VPN_HOST_NETWORK" ]; then
      for NW in $VPN_HOST_NETWORK; do
        ip route add to $NW via $DEFAULT_ROUTE_IP dev eth0
        iptables -A INPUT -s $NW -j ACCEPT
        iptables -A FORWARD -d $NW -j ACCEPT
        iptables -A FORWARD -s $NW -j ACCEPT
        iptables -A OUTPUT -d $NW -j ACCEPT
      done
    fi

    RETRY_COUNT=0
  else
    sleep 10
  fi
done
