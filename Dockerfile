FROM alpine:3.16

ENV LANG C.UTF-8
ENV VPN_ENABLE 1
ENV SOCKS5_ENABLE 0
ENV SOCKS5_FORKS 2
ENV SOCKS5_START_DELAY 5
ENV RETRY_MAX_COUNT 1
ENV STRONGSWAN_VERSION 5.5.1
ENV XL2TPD_VERSION 1.3.11
ENV WORK_DIR /tmp

RUN set -x && \
    apk add --no-cache \
              openrc \
              curl \
              bind-tools \
              ppp \
              dante-server \
    && apk add ca-certificates iproute2 iptables-dev openssl \
    && apk add build-base openssl-dev curl-dev libpcap-dev \
    && cd $WORK_DIR \
    && mkdir -p $WORK_DIR/strongswan \
    && wget https://download.strongswan.org/old/5.x/strongswan-$STRONGSWAN_VERSION.tar.bz2 -O $WORK_DIR/strongswan-$STRONGSWAN_VERSION.tar.bz2 \
    && tar --strip-components=1 -C $WORK_DIR/strongswan -xjf $WORK_DIR/strongswan*.tar.bz2 \
    && cd $WORK_DIR/strongswan \
    && ./configure --prefix=/usr \
                --sysconfdir=/etc \
                --libexecdir=/usr/lib \
                --with-ipsecdir=/usr/lib/strongswan \
                --enable-chapoly \
                --enable-cmd \
                --enable-curl \
                --enable-dhcp \
                --enable-openssl \
                --enable-farp \
                --enable-files \
                --enable-gcm \
                --enable-sha3 \
                --enable-shared \
                --disable-gmp \
    && make && make install \
    && cd $WORK_DIR \
    && wget https://github.com/xelerance/xl2tpd/archive/refs/tags/v$XL2TPD_VERSION.tar.gz -O $WORK_DIR/xl2tpd-$XL2TPD_VERSION.tar.gz \
    && tar -xzvf $WORK_DIR/xl2tpd-$XL2TPD_VERSION.tar.gz \
    && cd $WORK_DIR/xl2tpd-$XL2TPD_VERSION \
    && make && make install \
    && rm -rf $WORK_DIR/* \
    && apk del build-base openssl-dev curl-dev libpcap-dev \
    && mkdir -p /var/run/pluto \
    && mkdir -p /var/run/xl2tpd \
    && touch /var/run/xl2tpd/l2tp-control

# VPN Files
COPY ipsec.conf /etc/ipsec.conf
COPY ipsec.secrets /etc/ipsec.secrets
COPY xl2tpd.conf /etc/xl2tpd/xl2tpd.conf
COPY options.l2tpd.client /etc/ppp/options.l2tpd.client
# Socks5 Files
COPY sockd.conf /etc/sockd.conf
# Scripts
COPY startup.sh /
COPY reconnector.sh /

CMD ["/startup.sh"]
