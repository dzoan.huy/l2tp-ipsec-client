alpine-l2tp-vpn-client
---
[![pipeline status](https://gitlab.com/dzoan.huy/l2tp-ipsec-client/badges/main/pipeline.svg)](https://gitlab.com/dzoan.huy/l2tp-ipsec-client/-/commits/main)


An Alpine based docker image to setup an L2TP over IPsec VPN client w/ PSK (and optionally Socks5) within the container.
Using xl2tpd and strongswan.
Fork from https://github.com/WUAmin/alpine-l2tp-vpn-client


## Run
Setup environment variables for your credentials and config:

```bash
export VPN_SERVER='YOUR VPN SERVER IP OR FQDN'
export VPN_PSK='my pre shared key'
export VPN_USERNAME='myuser@myhost.com'
export VPN_PASSWORD='mypass'
export SOCKS5_ENABLE=0
```
Now run it (you can daemonize of course after debugging):
```bash
docker run --rm -it --privileged \
           -e VPN_SERVER \
           -e VPN_PSK \
           -e VPN_USERNAME \
           -e VPN_PASSWORD \
              dzoanhuy/l2tp-ipsec-client
```
You can use `.env` file:
```bash
source .env;docker run --rm -it --privileged --env-file .env -p ${SOCKS5_PORT}:1080 dzoanhuy/l2tp-ipsec-client
```

## Socks5
If you set `SOCKS5_ENABLE` to `1` (default value is `0`), the container will run `dante` at startup to provide a socks5 proxy (via VPN). Don not forget to expose port 1080.
```bash
export SOCKS5_ENABLE=0
export SOCKS5_PORT=1080
docker run --rm -it --privileged \
           -e VPN_SERVER \
           -e VPN_PSK \
           -e VPN_USERNAME \
           -e VPN_PASSWORD \
           -e SOCKS5_ENABLE \
           -p ${SOCKS5_PORT}:1080 \
              dzoanhuy/l2tp-ipsec-client
```


---

This project is forked from [l2tp-ipsec-vpn-client](https://github.com/ubergarm/l2tp-ipsec-vpn-client)
